<?php

namespace App\Controller;

use App\Classe\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GlobalController extends AbstractController
{
    /**
     * @Route("/", name="global")
     */
    public function index(Cart $cart): Response
    {
        return $this->render('global/index.html.twig', [
            'current_menu' => 'properties',
            'controller_name' => 'GlobalController',
            'totalQ'=> $cart->getQuantity()
        ]);
    }
}
