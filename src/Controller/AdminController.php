<?php

namespace App\Controller;

use App\Entity\Filterproduct;
use App\Entity\Product;
use App\Form\ProductFilterType;
use App\Form\ProductModifType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function index(Request $request, PaginatorInterface $paginator, ProductRepository $repo): Response
    {
        $recherche = new Filterproduct();
        $form = $this->createForm(ProductFilterType::class, $recherche);
        $form->handleRequest($request);
        

        $carte = $paginator->paginate(
        $repo->findAllWithPaginator($recherche), /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        6 /*limit per page*/
    );
        return $this->render('carte/index.html.twig', [
            'cartes' => $carte,
            'form' => $form->createView(),
            'admin' => true
        ]);
    }

    #[Route('/admin/creation', name: 'creationImmo')]
    #[Route('/admin/{id}', name: 'modif_admin', methods:['GET|POST'])]
    
    public function modif(Product $product= null, Request $request, EntityManagerInterface $em )
    {   
        if(!$product){
            $product = new Product();
        }
        $form = $this->createForm(ProductModifType::class,$product);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($product);
            $em->flush();
            $this->addFlash('success',"L'action a été éffectué");
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/modif.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            
        ]);

    }


    #[Route('/admin/{id}', name: 'supProduct', methods:['SUP'])]
    
    public function sup(Product $product, Request $request, EntityManagerInterface $em )
    {   
        if($this->isCsrfTokenValid("SUP".$product->getId(), $request->get("_token"))){
            $em->remove($product);
            $em->flush();
            $this->addFlash('success',"L'action a été éffectué");
            return $this->redirectToRoute('admin');
        }
    }
    
    
}
