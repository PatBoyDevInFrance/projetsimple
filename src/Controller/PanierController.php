<?php

namespace App\Controller;

use App\Classe\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PanierController extends AbstractController
{
    /**
     * @Route("/mon-panier", name="cart")
     */
    public function index(Cart $cart): Response
    {   
          
        return $this->render('panier/index.html.twig',[
            'cart' => $cart->getFullCart(),
            'total'=> $cart->getTotal(),
            'totalQ'=> $cart->getQuantity()
        ]);
    }

    /**
     * @Route("/cart/add/{id}", name="add_to_cart")
     */
    public function add($id, Cart $cart): Response
    {       
        $cart->add($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/remove/{id}", name="remove_to_cart")
     */
    public function remove($id, Cart $cart): Response
    {       
        $cart->remove($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/decrease/{id}", name="decrease_to_cart")
     */
    public function decrease ($id, Cart $cart): Response
    {       
        $cart->decrease($id);

        return $this->redirectToRoute('cart');
    }


}
