<?php

namespace App\Controller;

use App\Entity\Filterproduct;
use App\Form\ProductFilterType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarteController extends AbstractController
{
    /**
     * @Route("/carte", name="carte_index")
     */
    public function index(ProductRepository $repo, PaginatorInterface $paginator, Request $request): Response
    {   
        $recherche = new Filterproduct();
        $form = $this->createForm(ProductFilterType::class, $recherche);
        $form->handleRequest($request);
        

        $carte = $paginator->paginate(
        $repo->findAllWithPaginator($recherche), /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        6 /*limit per page*/
    );
        return $this->render('carte/index.html.twig', [
            'current_menu' => 'properties',
            'cartes' => $carte,
            'form' => $form->createView(),
            'admin' => false
        ]);
    }

    

}
