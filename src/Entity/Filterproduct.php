<?php

namespace App\Entity;

class Filterproduct {

    private $minPrix ;
    private $maxPrix;

    function getMinPrix(){return $this->minPrix;}
    function setMinPrix(int $prix){$this->minPrix = $prix; return $this;}

    function getMaxPrix(){return $this->maxPrix;}
    function setMaxPrix(int $prix){$this->maxPrix = $prix; return $this;}

}